import pymysql.cursors
import sys

name = sys.argv[1]


# Connect to the database
connection = pymysql.connect(host='localhost',
                            user='root',
                            password='',
                            db='daw',
                            charset='utf8mb4',
                            cursorclass=pymysql.cursors.DictCursor)

try:
    with connection.cursor() as cursor:
        # Create a new record
        sql = "DELETE FROM `user` WHERE name='" + name + "'"
        cursor.execute(sql)

    # connection is not autocommit by default. So you must commit to save
    # your changes.
    connection.commit()

    #with connection.cursor() as cursor:
    # Read a single record
    #   sql = "SELECT `id`, `password` FROM `users` WHERE `email`=%s"
    #  cursor.execute(sql, ('webmaster@python.org',))
    # result = cursor.fetchone()
    #print(result)
    print("success")
finally:
    connection.close()