import pymysql.cursors
import sys

commentId = sys.argv[1]
imgName = sys.argv[2]

# Connect to the database
connection = pymysql.connect(host='localhost',
                            user='root',
                            password='',
                            db='daw',
                            charset='utf8mb4',
                            cursorclass=pymysql.cursors.DictCursor)

try:
    with connection.cursor() as cursor:
        # Create a new record
        id_index = int(commentId)-1
        sql = "delete from news_comment where id=(SELECT id FROM (select * from news_comment where associated_jpg='" + imgName + "') AS T ORDER BY ID LIMIT " +  str(id_index) +",1)"
        cursor.execute(sql)    
    # connection is not autocommit by default. So you must commit to save
    # your changes.
    connection.commit()

    #with connection.cursor() as cursor:
    # Read a single record
    #   sql = "SELECT `id`, `password` FROM `users` WHERE `email`=%s"
    #  cursor.execute(sql, ('webmaster@python.org',))
    # result = cursor.fetchone()
    #print(result)
    print("success")
finally:
    connection.close()