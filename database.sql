create database daw;
use daw;

CREATE TABLE `daw`.`domain` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

 CREATE TABLE `daw`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `pass` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `age` VARCHAR(45) NULL,
  `sex` VARCHAR(45) NULL,
  `interest_domain` VARCHAR(45) NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `daw`.`news_comment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `comment` VARCHAR(45) NULL,
  `associated_jpg` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));


ALTER TABLE `daw`.`user` 
ADD COLUMN `profile_image` BLOB NULL AFTER `interest_domain`;

SELECT COUNT(*) FROM user WHERE(name='Ovidiu' AND pass='oviovi');


INSERT INTO `daw`.`domain` (`name`, `description`) VALUES ('IT', 'software related domain');
INSERT INTO `daw`.`domain` (`name`, `description`) VALUES ('Mechanics', 'Technical and practical domain in applied mechanics');
INSERT INTO `daw`.`domain` (`name`, `description`) VALUES ('Sport', 'Athletic events and competitions');

INSERT INTO `daw`.`user` (`name`, `pass`, `email`, `age`, `sex`, `interest_domain`) VALUES ('Ovidiu', 'oviovi', 'cristurean_ovidiu@yahoo.com', '22', 'male', 'Athletic events and competitions');
INSERT INTO `daw`.`user` (`name`, `pass`, `email`, `age`, `sex`, `interest_domain`) VALUES ('Dorin', 'dordor', 'dorin@dorinescu.ro', '22', 'male', 'IT/Software Engineering');

select name, comment from news_comment where associated_jpg='connor.jpg';
;

select count(*) from news_comment where associated_jpg='connor.jpg';

insert into news_comment(id, name, comment, associated_jpg) values (null, 'Dorin', 'Seems fair to me', 'trump.jpg');
INSERT INTO `daw`.`news_comment` (`name`, `comment`, `associated_jpg`) VALUES ('Ovidiu', 'I am right ', 'connor.jpg');


ALTER TABLE `daw`.`user` 
ADD COLUMN `role` VARCHAR(45) NULL AFTER `profile_image`;

UPDATE `daw`.`user` SET `role`='user' WHERE `id`='1';
UPDATE `daw`.`user` SET `role`='admin' WHERE `id`='2';

insert into user(id, `name`, pass, email, age, sex, interest_domain, profile_image, role) values (null, 'unnume', 'oparola', null, null, null, null, null, 'user');

SET SQL_SAFE_UPDATES = 0;

update user set name='Dorin', age=null where name='dorinnn';

delete from user where name='unnume';

delete from news_comment where id=(
SELECT id FROM 
  (select * from news_comment where associated_jpg='connor.jpg') AS T
 ORDER BY ID LIMIT 1,1
 );
 

select * from news_comment where associated_jpg='connor.jpg';

delete from news_comment where id=(SELECT id FROM (select * from news_comment where associated_jpg='connor.jpg') AS T ORDER BY ID LIMIT 0,1);

update news_comment set comment='Comment hidden by admin'
where id=(SELECT id FROM (select * from news_comment where associated_jpg='connor.jpg') AS T ORDER BY ID LIMIT 0,1);

select * from domain;
select * from user;
select * from news_comment;


delete from news_comment where id=(SELECT id FROM (select * from news_comment where associated_jpg='connor.jpg') AS T ORDER BY ID LIMIT 0,1)