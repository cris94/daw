
$(document).ready(function(){
$("#finish-login-btn").on('click', function(){
		var name = $("#name").val();
		var pass = $("#pass").val();

		var formData = new FormData();
		formData.append('name',name);
		formData.append('pass',pass);
		
		//Validari aici;
		$.ajax({
            url: "controller/login.php",
            type: "POST",
			async: false,
			contentType : false,       
			cache       : false,             
			processData : false,
			enctype     : 'multipart/form-data',  
            data: formData,
            success: function (msg) {
               if(msg == "user"){
				   //location.reload();
				   location.href="personal_profile.php";
			   } else if(msg=="admin") {
			   		alert("You are now logged in as an admin");
			   		location.href="admin_modify.php";
			   } else{
				   alert("Login error. Invalid username or password");
			   }
            }
        });
	});
});