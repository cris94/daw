<?PHP
session_start();
?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login Form</title>
  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  <link rel="stylesheet" href="header-login-signup.css">
  <link href='http://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="footer-distributed-with-contact-form.css">



  
</head>

<body>
  <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '296456504100919',
        cookie     : true,
        xfbml      : true,
        version    : 'v2.8'
      });
      FB.AppEvents.logPageView();   
    };

    (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

    function checkLoginState() {
      FB.getLoginStatus(function(response) {
        if(response.status=="connected") {
          alert("You just logged in through facebook");
          getEmailFromFb("onLogin");
          //check if the email is inside 
        }
        statusChangeCallback(response);
      });
    }

    function checkRegisterState() {
      FB.getLoginStatus(function(response) {
        alert(response.status);
        if(response.status=="connected") {
          alert("registered");
          getEmailFromFb("onRegister");
        }
        statusChangeCallback(response);
      });
    }

    function getEmailFromFb(onAction) {
      FB.api('/me', { locale: 'en_US', fields: 'name, email' },
        function(response) {
          //check if the email is inside the db
          checkEmailInDb(response.name, response.email, onAction);
        }
        );

    }

    function checkEmailInDb(name, email, onAction) {
      var formData = new FormData();
      formData.append('name', name);
      formData.append('email',email);

      $.ajax({
        url: "controller/checkEmail.php",
        type: "POST",
        async: false,
        contentType : false,       
        cache       : false,             
        processData : false,
        enctype     : 'multipart/form-data',  
        data: formData,
        success: function (msg) {
         //alert("Welcome back, " + msg);
         //location.href="main_page.php";
         //alert(msg);
         var resp = JSON.parse(msg);

         if(onAction=="onLogin") {
          if(resp.role=="user") {
            location.href="main_page.php";
          }

          if(resp.role=="admin") {
            location.href="admin_modify.php";
          }
        } else if(onAction=="onRegister") {
          if(resp.role!="") {
            alert("You already have an account");
          } else {
              //create an account for the newly registered user
              registerUserInApplication(name, email);
            }
          }

        }
      });
    }

    function registerUserInApplication(name, email) {
      var formData = new FormData();
      formData.append('name', name);
      formData.append('email', email);
      
      $.ajax({
        url: "controller/registerUser.php",
        type: "POST",
        async: false,
        contentType : false,       
        cache       : false,             
        processData : false,
        enctype     : 'multipart/form-data',  
        data: formData,
        success: function (msg) {
          alert("register in db response: " + msg);
        }
      });
    }
  </script>

  <header class="header-login-signup">

    <div class="header-limiter">

      <h1><a href="#">Personal<span>presentation</span></a></h1>

    </div>

  </header>

  <div class="form">

    <div class="">
      <div id="">   
        <h1>Login</h1>

        <form>
          <div class="field-wrap">
            <input id="name" autocomplete="off"/>
          </div>

          <div class="field-wrap">
           <input  type = "password" id="pass" autocomplete="off"/>

         </div>

         <input type="checkbox" name="vehicle" value="Bike">Admin

         <button type="button" id="finish-login-btn" class="button button-block"/>Login</button>

         <h1>Login with facebook:</h1>
         <fb:login-button 
         scope="public_profile,email"
         onlogin="checkLoginState();">
       </fb:login-button>

       <h1>Register for an account with facebook</h1>
       <fb:login-button 
       scope="public_profile,email"
       onlogin="checkRegisterState();">
     </form> 


   </div>


 </div>

</div>  

<footer class="footer-distributed">
  <div class="footer-left">
    <h3>Company<span>logo</span></h3>
  </div>

</footer>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src = 'login.js'></script>

  <!--   <script src="../../js/index.js"></script>
-->
</body>
</html>