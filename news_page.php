  <?PHP

  session_start();


  ?>
  <!DOCTYPE html>
  <html >
  <head>
    <meta charset="UTF-8">
    <title>Sign-Up/Login Form</title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

    <link rel="stylesheet" href="header-user-dropdown.css">
    <link href='http://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="footer-distributed-with-contact-form.css">

    <!-- <link rel="stylesheet" href="http://institut-de-genomique.github.io/Ultimate-DataTable/js/bootstrap/css/bootstrap-3.3.4.min.css">
    <link rel="stylesheet" href="http://institut-de-genomique.github.io/Ultimate-DataTable/js/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://institut-de-genomique.github.io/Ultimate-DataTable/css/ultimate-datatable-3.3.1-SNAPSHOT.css"> -->

    <script type="text/javascript" src="angular.min.js"></script>
    <script type="text/javascript" src="main.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>


    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script type="text/javascript">
      var languagePref = localStorage.getItem("lang");
      var xmlFile;
      if(languagePref=="en") {
        xmlFile="news.xml";
      } else {
        xmlFile="news_ro.xml";
      }
      $(document).ready(function(){
        $.ajax({
          type: "GET",
          url: xmlFile,
          dataType: "xml",
          success: xmlParser
        });
      });

      function xmlParser(xml) {
        $('#load').fadeOut();
        var i=0;
        $(xml).find("painting").each(function(){
          if(i==parseInt(localStorage.getItem("itemIndex"))) {
            $("#container").append('<div class="painting"><img src="images/' + $(this).find("news_img").text() + '"width="200" height="225" alt="' + $(this).find("title").text() + '" /><br/><div class="custom-news-dropdown">'+ "<b>Title:</b>" + $(this).find("title").text() + '<br/>'+ "<b>Message:</b> " + $(this).find("price").text()  + '</div>' +'</div>');
            $(".painting").fadeIn(1000);

            //save jpg name in local storage
            localStorage.setItem("img_name", $(this).find("news_img").text());
          }
          i++;
        });
      }
    </script> 

    <script type="text/javascript">
      var img_name = localStorage.getItem("img_name");
      var formData = new FormData();
      formData.append('img_name',img_name);
      $(document).ready(function(){
        $.ajax({
          url: "controller/getComments.php",
          type: "POST",
          async: false,
          contentType : false,       
          cache       : false,             
          processData : false,
          enctype     : 'multipart/form-data',  
          data: formData, 
          success: getComments
        });
      });

      function getComments($msg) {
        var jsonData = JSON.parse($msg);
        var ul = document.getElementById("populateComments");
        for( var i = 0; i < jsonData.records.length; i++ )
        { 
          o = jsonData.records[i];
          var li = document.createElement("li");

          var nameTf = document.createTextNode(o.name + "   :   ");
          //nameTf.setAttribute("style", "background-color: red;");
          li.appendChild(nameTf);

          var commentTf = document.createTextNode(o.comment);
          //commentTf.setAttribute("style", "#ffffff");
          li.appendChild(commentTf);

          var btn = document.createElement("BUTTON"); 
          var t = document.createTextNode("Delete");

          //btn.setAttribute("id", ""+i);
          btn.appendChild(t);
          /*btn.onclick = function(){
              alert("you pressed on the " + i +" button");return false;
            };*/
            li.appendChild(btn);

            ul.appendChild(li);    
          }  
          $('.giveId').on('click', 'li', function(e){
            if($(this).index() > 4) {
              deleteComment($(this).index());
            }

          });

          function deleteComment(index) {

            var formData = new FormData();
            formData.append('index',index-4);

            var img_name = localStorage.getItem("img_name");
            formData.append('imgName', img_name);

            var endpoint;
            if(isHideEnabled()) {
              endpoint="hideComment.php";
            } else {
              endpoint="deleteComment.php";
            }

    //Validari aici;
    $.ajax({
      url: "controller/" + endpoint,
      type: "POST",
      async: false,
      contentType : false,       
      cache       : false,             
      processData : false,
      enctype     : 'multipart/form-data',  
      data: formData,
      success: function (msg) {
       /*if(msg == "success"){
         alert("Comment deleted");
       } else{
         alert(msg);
       }*/
       alert(msg);
     }
   });
  }
}

</script>

<script type="text/javascript">
  function isHideEnabled() {
    if(document.getElementById('isHideEnabled').checked) {
      return true;
    } else {
      return false;
    }
  }
</script>

</head>
<header class="header-user-dropdown">

  <div class="header-limiter">
    <h1><a href="index.html">Personal<span>presentation</span></a></h1>

    <div class="header-user-menu">Contact
     <ul>
      <li><a href="contact.html">Formular</a></li>
    </ul>
  </div>

  <div class="header-user-menu">Limba
   <ul>
    <li><a href="." id="language_ro">Romana</a></li>
    <li><a href="." id="language">English</a></li>
  </ul>
</div>

</nav>

</div>


</header>


</header>
<div class="form">
  <div id="container">
    <div align = "center" class="loader"><img src="images/loading.gif" id="load" width="400" height="400" align="absmiddle"/>

    </div>

  </div>

</div>

<div class="form">
  <div id="comments">
    <!-- get comments from db -->

    <ul id="populateComments" class="giveId" style="font-weight: bold;">
      <li><input id="user_name" type="text" value="<?=$_SESSION['user_name'];?>"  autocomplete="off"/></li>
      <li><a href="contact.html"><b>Comentarii</b></a></li>
      <li><input id="your_comment" type="text" name="your_comment" size="100"></li>
      <li><button type="button" id="insert-comment-btn" class="button button-block"/>Leave a comment</button></li>
      <li><input type="checkbox" id="isHideEnabled" name="hideComm" value="hide">Hide comment</li>

    </ul>

  </div>
</div> 


</div>  <!-- /form -->

<div ng-controller="demoController">
  <div class="col-md-12 col-lg-12" ultimate-datatable="datatable">
  </div>
</div>

<footer class="footer-distributed">
 <div class="footer-left">
  <h3>Helloooo<span>page</span></h3>
</div>

<script src = 'employee.js'></script>

</footer>


<script src = 'goToNews.js'></script>
<script src = 'language.js'></script>
<script src = 'insertComment.js'></script>
<script src = 'deleteComment'></script>

    <!--   <script src="../../js/index.js"></script>
  -->
</body>
</html>