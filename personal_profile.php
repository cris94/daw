<?PHP

//retrieve domain of interest
session_start();


?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Sign-Up/Login Form</title>
  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  <link rel="stylesheet" href="header-user-dropdown.css">
  <link href='http://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="footer-distributed-with-contact-form.css">
  
</head>

<body>
  <header class="header-user-dropdown">

    <div class="header-limiter">
      <h1><a href="index.html">Personal<span>presentation</span></a></h1>

      <div class="header-user-menu">Your profile
       <ul>
        <li><a href="#">Formular</a></li>
      </ul>
    </div>

    <div class="header-user-menu">Limba
     <ul>
      <li><a href="." id="language_ro">Romana</a></li>
      <li><a href="." id="language">English</a></li>
    </ul>
  </div>


</div>

</header>

<div class="form">

  <div class="">
    <div id="signup">   
      <h1>Your personal details</h1>

      <form action="/" method="post">
        <div>
          <div><b>Name:</b></div>
          <input id="name" type="text" value="<?=$_SESSION['user_name'];?>"  autocomplete="off" />
        </div>

        <br><br>

        <div>
          <div><b>Email:</b></div>
          <input id="email" type="text" value="<?=$_SESSION['user_email'];?>" autocomplete="off"/>
        </div>

        <br><br>

        <div>
         <div><b>Age:</b></div>
         <input id="age" value="<?=$_SESSION['user_age'];?>"></input>
       </div>

       <br><br>

       <div>
         <div><b>Sex:</b></div>
         <input name="sex" autocomplete="off" value="<?=$_SESSION['user_sex'];?>"/>
       </div>

       <br><br>

       <div>
        <div><b>Domain of interest:</b></div>
        <input id="domain" autocomplete="off" value="<?=$_SESSION['user_interest'];?>"/>
      </div>

      <br><br>

      <button type="submit" id="call-mail-btn" class="button button-block"/>Subscribe to news</button>

    </form>

  </div>


</div><!-- tab-content -->

</div> <!-- /form -->

<footer class="footer-distributed">

 <div class="footer-left">

  <h3>Personal<span>presentation</span></h3>
</div>



</footer>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script src = 'employee.js'></script>
<script src = 'language.js'></script>
<script src = 'callMail.js'></script>
  <!--   <script src="../../js/index.js"></script>
-->
</body>
</html>