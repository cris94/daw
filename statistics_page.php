<?PHP
session_start();
?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login Form</title>
  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  <link rel="stylesheet" href="header-login-signup.css">
  <link href='http://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="footer-distributed-with-contact-form.css">

  <script type="text/javascript" src="angular.min.js"></script>
  <script type="text/javascript" src="main.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>


  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      var jsonData;
      var x=[];
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        $.ajax({
          url: "controller/getStatsComments.php",
          type: "POST",
          async: false,
          contentType : false,       
          cache       : false,             
          processData : false,
          enctype     : 'multipart/form-data',  
          success: function (msg) {
            jsonData = JSON.parse(msg);
            var inregistrari=[];
            for( var i = 0; i < jsonData.records.length; i++ ) {
              //alert(jsonData.records[i].name + " " + jsonData.records[i].comment_count);
            }

            var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          [jsonData.records[0].name,     parseInt(jsonData.records[0].comment_count)],
          [jsonData.records[1].name,      parseInt(jsonData.records[1].comment_count)],
          [jsonData.records[2].name,      parseInt(jsonData.records[2].comment_count)]
          ]);


            var options = {
              title: 'User comments is news'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
            /*var data = new google.visualization.DataTable(jsonData);
            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, {width: 400, height: 240});*/
            drawBasic();
          }
        });

        }

        function drawBasic() {
          $.ajax({
          url: "controller/getNewsByMonth.php",
          type: "POST",
          async: false,
          contentType : false,       
          cache       : false,             
          processData : false,
          enctype     : 'multipart/form-data',  
          success: function (msg) {
            jsonData = JSON.parse(msg);
          }
        });

      var data = new google.visualization.DataTable();
      data.addColumn('timeofday', 'Month');
      data.addColumn('number', 'Number of posted news');

      data.addRows([
        [{v: [1, 0, 0], f: 'Ianuarie'}, 0],
        [{v: [2, 0, 0], f: 'Februarie'}, 0],
        [{v: [3, 0, 0], f:'Martie'}, 0],
        [{v: [4, 0, 0], f: 'Aprilie'}, parseInt(jsonData.records[0].associated_jpg)],
        [{v: [5, 0, 0], f: 'Mai'}, parseInt(jsonData.records[1].associated_jpg)],
        [{v: [6, 0, 0], f: 'Iunie'}, 0],
        [{v: [7, 0, 0], f: 'Iulie'}, 0],
        [{v: [8, 0, 0], f: 'August'}, 0],
        [{v: [9, 0, 0], f: 'Septembrie'}, 0],
        [{v: [10, 0, 0], f: 'Octombrie'}, 0],
      ]);

      var options = {
        title: 'Number of news posted per month',
        hAxis: {
          title: 'Month',
          format: 'h',
          viewWindow: {
            min: [1, 0, 0],
            max: [12, 0, 0]
          }
        },
        vAxis: {
          title: 'Number of news'
        }
      };

      var chart = new google.visualization.ColumnChart(
        document.getElementById('chart_div'));

      chart.draw(data, options);
    }
      });
    </script>

  </head>

  <body>

    <div class="form">
      <h1>Site statistics</h1>
      <div id="piechart" style="width: 900px; height: 500px;"></div>
      <br/><br/><br/>
      <div id="chart_div"></div>
    </div> 
  </body>
  </html>